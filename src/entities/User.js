"use strict";

class User{

    constructor(login = 'test465465alpha',
                fullName = 'Vins Order',
                password = '12345678',
                email = 'test@mail.com'){
        this._login = login;
        this._fullName = fullName;
        this._password = password;
        this._email = email;
    }

    get login() {
        return this._login;
    }

    set login(value) {
        this._login = value;
    }

    get fullName() {
        return this._fullName;
    }

    set fullName(value) {
        this._fullName = value;
    }

    get password() {
        return this._password;
    }

    set password(value) {
        this._password = value;
    }

    get email() {
        return this._email;
    }

    set email(value) {
        this._email = value;
    }
}

module.exports = User;
