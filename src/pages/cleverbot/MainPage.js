'use strict';

let BasePage = require('./../BasePage');
let SignupModule = require('./modules/SignupModule');
let LoginModule = require('./modules/LoginModule');

class MainPage extends BasePage{

    get logo()                  { return $('#cleverbotlogo'); }
    get signInTab()             { return $('#cbsocialsigninup'); }
    get botResponse()           { return $('#line1 .bot'); }
    get sayField()              { return $('.stimulus');}
    get thinkAboutItButton()    { return $('input[name="thinkaboutitbutton"]'); }
    get userName()              { return $('#cbsocialsigninup span:first-child'); }

    constructor(){
        super();
        this.signupModule = new SignupModule('#cbsocialsignupform');
        this.loginModule = new LoginModule('form[onsubmit="socialAction.submitForm(\'signin\',this); return false;"]');
    }

    unfocus(){
        this.logo.click();
    }

    open(){
        super.open('/');
        super.isLoaded(this.logo);
    }

    signUp(user){
        this.signInTab.click();
        this.signupModule.isLoaded();
        console.log(user);
        this.signupModule.signUp(user);
    }

    signIn(user){
        this.loginModule.isLoaded();
        this.loginModule.signIn(user);
    }

    sayMessage(text){
        let i = 0;
        while (i < 12){
            try{
                this.sayField.waitForVisible();
                this.sayField.setValue(text);
            }catch (err){
                console.log(err.message);
                browser.pause(1000);
            }
            i++;
        }
        this.thinkAboutItButton.click();
    }

    getBotResponse(){
        this.botResponse.waitForVisible();
        return this.botResponse.getText();
    }

    isLoggedIn(user){
        this.userName.waitForVisible();
        console.log(this.userName.getText());
        return this.userName.getText() === user.login;
    }

}

module.exports = MainPage;
