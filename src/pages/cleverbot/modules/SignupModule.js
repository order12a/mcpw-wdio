'use strict';

let BaseModule = require('./BaseModule');

class SignupModule extends BaseModule{

    get usernameField()         { return this.innerElement.$('input[name="username"]'); }
    get fullNameField()         { return this.innerElement.$('input[name="fullname"]'); }
    get emailField()            { return this.innerElement.$('input[name="email"]'); }
    get passwordClearField()    { return this.innerElement.$('.passwordclear'); }
    get passwordField()         { return this.innerElement.$('.passwordnormal'); }
    get agreementSelect()       { return this.innerElement.$('#cbsocialregisterterms'); }
    get signUpButton()          { return this.innerElement.$('.standout'); }

    constructor(locator){
        super(locator);
    }

    signUp(user){
        this.usernameField.setValue(user.login);
        this.fullNameField.setValue(user.fullName);
        this.emailField.setValue(user.email);
        this.passwordClearField.click();
        this.passwordField.setValue(user.password);
        this.agreementSelect.selectByValue('yes');
        this.signUpButton.click();
    }
}

module.exports = SignupModule;
