'use strict';

let BaseModule = require('./BaseModule');

class LoginModule extends BaseModule{

    get usernameField()             { return this.innerElement.$('input[name="username"]'); }
    get passwordClearField()        { return this.innerElement.$('.passwordclear'); }
    get passwordField()             { return this.innerElement.$('.passwordnormal'); }
    get signInButton()              { return this.innerElement.$('input[type="submit"]'); }

    constructor(locator){
        super(locator);
    }

    signIn(user){
        this.usernameField.setValue(user.email);
        this.passwordClearField.click();
        this.passwordField.setValue(user.password);
        this.signInButton.click();
    }
}

module.exports = LoginModule;

