"use strict";

class BaseModule{
    get innerElement() { return $(this.locator); }

    constructor(locator){
        this.locator = locator;
    }

    isLoaded(){
        $(this.locator).waitForVisible();
        return $(this.locator).isVisible();
    }
}

module.exports = BaseModule;