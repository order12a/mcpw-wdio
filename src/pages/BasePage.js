"use strict";

class BasePage{
    constructor(){
    }

    open(path){
        browser.url(path);
    }

    isLoaded(element){
        element.waitForVisible();
        return element.isVisible();
    }
}

module.exports = BasePage;
