"use strict";

let tempamilStartPage = require('./tempmail/TempmailStartPage');
let MailPage = require('./tempmail/MailPage');
let MainPage = require('./cleverbot/MainPage');

class PageManager{
    constructor(){
        this.tempmailStartPage = new tempamilStartPage();
        this.mailPage = new MailPage();
        this.mainPage = new MainPage();
    }
}

module.exports = PageManager;