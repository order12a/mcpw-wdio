'use strict';

let BasePage = require('./../BasePage');

class TempmailStartPage extends BasePage{

    get startButton()             { return $('.orange');   }
    get header()                  { return $('.header');  }

    constructor(){
        super();
    }

    start(){
        this.startButton.click();
    }

    open(){
        super.open('https://mytemp.email/');
    }

    isLoaded(){
        super.isLoaded(this.header);
    }
}

module.exports = TempmailStartPage;
