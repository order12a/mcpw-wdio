'use strict';

let BasePage = require('./../BasePage');

class MailPage extends BasePage{

    get mailAddress()       { return $('.inbox-status .md-list-item-inner .truncate'); }
    get logo()              { return $('.xlogged'); }
    get deleteMailbox()     { return $('button[aria-label="destroy"] svg'); }
    get mail()              { return $('.emls .eml-status-unread'); }
    get deleteEmail()       { return $('button[aria-label="destroy email"] md-icon'); }
    get mailFrame()         { return $('#body-html-iframe-content'); }
    get cleverbotLink()     { return $('#inner a'); }

    constructor(){
        super();
    }

    isLoaded(){
        return super.isLoaded(this.logo);
    }

    getCurrentUrl(){
        return browser.getUrl();
    }

    getEmailAddress(){
        this.mailAddress.waitForVisible();
        return this.mailAddress.getText();
    }

    navigateInbox(){
        this.mailAddress.waitForVisible();
        this.mailAddress.click();
    }

    destroyCurrentMailAddress(){
        this.deleteMailbox.click();
    }

    remoweMail(){
        this.openEmail();
        this.deleteEmail.waitForVisible();
        this.deleteEmail.click();
    }

    getCleverbotActivationLink(){
        this.openEmail();
        this.mailFrame.waitForExist();
        browser.frame(this.mailFrame.value);
        this.cleverbotLink.waitForVisible();
        return this.cleverbotLink.getText();
    }

    openEmail(){
        this.mail.waitForVisible();
        this.mail.click();
    }

    open(){
        super.open('https://mytemp.email/2/#!/');
        super.isLoaded(this.logo);
    }
}

module.exports = MailPage;
