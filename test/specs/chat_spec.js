"use strict";
let expect = require('chai').expect;

let User = require('./../../src/entities/User');
let PageManager = require('../../src/pages/PageManager');

describe('Complete E2E scenario', () => {
    let pages = new PageManager();
    let user = new User();
    let activationLink;

    before(function () {
        user.login = 'Tester' + Math.floor((Math.random() * 10005000) + 1);
        pages.mailPage.open();
        user.email = pages.mailPage.getEmailAddress();
        pages.mailPage.remoweMail();
    });

    it('should signup to cleverbot', function () {
        pages.mainPage.open();
        pages.mainPage.signUp(user);
    });
    
    it('should extract activation link', function () {
        pages.mailPage.open();
        pages.mailPage.navigateInbox();
        activationLink = 'https://' + pages.mailPage.getCleverbotActivationLink();
        expect(activationLink).to.be.a('string');
    });

    it('should login to cleverbot', function () {
        browser.url(activationLink);
        pages.mainPage.signIn(user);
        expect(pages.mainPage.isLoggedIn(user)).to.be.true;
    });

    it('should chat with the bot', function () {
        pages.mainPage.sayMessage('Test');
        expect(pages.mainPage.getBotResponse()).to.be.a('string');
        expect('foo').to.have.length.above(1);

        pages.mainPage.sayMessage('Say');
        expect(pages.mainPage.getBotResponse()).to.be.a('string');
        expect('foo').to.have.length.above(1);

        pages.mainPage.sayMessage('Second');
        expect(pages.mainPage.getBotResponse()).to.be.a('string');
        expect('foo').to.have.length.above(1);

    });
});
